package com.example.maintenance.domain.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Created by lkpiel on 2/18/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class MaintenancePlan {
    @Id
    String id;
    Integer yearOfAction;

    @OneToMany(fetch = FetchType.EAGER, cascade={CascadeType.ALL})
    List<MaintenanceTask> tasks;
    String plant;
}
