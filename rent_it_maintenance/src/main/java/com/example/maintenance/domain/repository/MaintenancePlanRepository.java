package com.example.maintenance.domain.repository;

import com.example.maintenance.domain.model.MaintenancePlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by lkpiel on 2/18/2017.
 */
@Repository
public interface MaintenancePlanRepository  extends JpaRepository<MaintenancePlan, String> {
    @Query("SELECT plan FROM MaintenancePlan plan WHERE plan.yearOfAction = ?1")
    MaintenancePlan findOneByYear(int year);
}
