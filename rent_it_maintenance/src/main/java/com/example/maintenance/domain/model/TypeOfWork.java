package com.example.maintenance.domain.model;

/**
 * Created by lkpiel on 2/18/2017.
 */
public enum TypeOfWork {
    PREVENTIVE, CORRECTIVE, OPERATIVE
}
