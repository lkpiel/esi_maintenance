package com.example.maintenance.domain.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by lkpiel on 2/18/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class MaintenanceTask {
    @Id
    String id;
    String description;
    @Enumerated(EnumType.STRING)
    TypeOfWork typeOfWork;
    @Column(precision = 8, scale = 2)
    BigDecimal price;

}
