package com.example.maintenance.domain.repository;

import com.example.maintenance.domain.model.MaintenanceTask;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by lkpiel on 2/18/2017.
 */
public interface MaintenanceTaskRepository  extends JpaRepository<MaintenanceTask, String> {

}
