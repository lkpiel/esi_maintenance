package com.example.maintenance.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by Philosoraptor on 06/03/2017.
 */
@Service
public class MaintenanceIdentifierFactory {
    public String nextMaintenanceID() {
        return UUID.randomUUID().toString();
    }
}