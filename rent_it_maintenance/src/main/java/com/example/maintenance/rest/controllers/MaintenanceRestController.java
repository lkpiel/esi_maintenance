package com.example.maintenance.rest.controllers;

import com.example.maintenance.application.dto.MaintenanceTaskDTO;
import com.example.maintenance.application.services.MaintenanceService;
import com.example.maintenance.application.services.MaintenanceTaskAssembler;
import com.example.maintenance.domain.repository.MaintenanceTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

/**
 * Created by lkpiel on 5/24/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/maintenance")
public class MaintenanceRestController {
    @Autowired
    MaintenanceService maintenanceService;
    @Autowired
    MaintenanceTaskAssembler maintenanceTaskAssembler;
    @Autowired
    MaintenanceTaskRepository maintenanceTaskRepository;

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public MaintenanceTaskDTO planMaintenanceTask(@RequestBody MaintenanceTaskDTO maintenanceTaskDTO) throws IOException, MessagingException {
        MaintenanceTaskDTO returnMaintenanceTaskDTO = maintenanceService.createMaintenanceTask(maintenanceTaskDTO);
        return returnMaintenanceTaskDTO;
    }
    @GetMapping("/{id}")
    public MaintenanceTaskDTO findMaintenanceTask(
            @PathVariable(value = "id") String id
    ) {
        return maintenanceTaskAssembler.toResource(maintenanceTaskRepository.findOne(id));
    }
    @GetMapping("")
    public List<MaintenanceTaskDTO> findAllMaintenanceTasks(){
        return maintenanceTaskAssembler.toResources(maintenanceTaskRepository.findAll());
    }
}
