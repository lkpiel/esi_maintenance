package com.example.maintenance.application.services;

import com.example.maintenance.application.dto.MaintenanceTaskDTO;
import com.example.maintenance.domain.model.MaintenanceTask;
import com.example.maintenance.rest.controllers.MaintenanceRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Service
public class MaintenanceTaskAssembler  extends ResourceAssemblerSupport<MaintenanceTask, MaintenanceTaskDTO> {

    public MaintenanceTaskAssembler() {
        super(MaintenanceRestController.class, MaintenanceTaskDTO.class);
    }

    @Override
    public MaintenanceTaskDTO toResource(MaintenanceTask mt) {
        if(mt != null) {
            MaintenanceTaskDTO dto = createResourceWithId(mt.getId(), mt);
            dto.set_id(mt.getId());
            dto.setPrice(mt.getPrice());
            dto.setDescription(mt.getDescription());
            dto.setTypeOfWork(mt.getTypeOfWork());
            return dto;
        }else {
            return null;
        }
    }
    public MaintenanceTask toEntity(MaintenanceTaskDTO maintenanceTaskDTO) {
        MaintenanceTask maintenanceTask = MaintenanceTask.of(maintenanceTaskDTO.get_id(),maintenanceTaskDTO.getDescription(),maintenanceTaskDTO.getTypeOfWork()
        ,maintenanceTaskDTO.getPrice());
        return maintenanceTask;
    }
    public List<MaintenanceTaskDTO> toResources(List<MaintenanceTask> maintenanceTasks) {
        if(maintenanceTasks.size() < 1){
            return new ArrayList<MaintenanceTaskDTO>();
        }
        return maintenanceTasks.stream().map(p -> toResource(p)).collect(Collectors.toList());
    }
    public List<MaintenanceTask> toEntities(List<MaintenanceTaskDTO> maintenanceTaskDTOS) {
        if(maintenanceTaskDTOS.size() < 1){
            return new ArrayList<MaintenanceTask>();
        }
        return maintenanceTaskDTOS.stream().map(p -> toEntity(p)).collect(Collectors.toList());
    }


}
