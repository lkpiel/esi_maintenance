package com.example.maintenance.application.services;

import com.example.maintenance.application.dto.MaintenancePlanDTO;
import com.example.maintenance.domain.model.MaintenancePlan;
import com.example.maintenance.rest.controllers.MaintenanceRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Service
public class MaintenancePlanAssembler  extends ResourceAssemblerSupport<MaintenancePlan, MaintenancePlanDTO> {

    public MaintenancePlanAssembler() {
        super(MaintenanceRestController.class, MaintenancePlanDTO.class);
    }

    @Autowired
    MaintenanceTaskAssembler maintenanceTaskAssembler;
    @Override
    public MaintenancePlanDTO toResource(MaintenancePlan mp) {
        if(mp != null) {
            MaintenancePlanDTO dto = createResourceWithId(mp.getId(), mp);
            dto.set_id(mp.getId());
            dto.setPlant(mp.getPlant());
            dto.setTasks(maintenanceTaskAssembler.toResources(mp.getTasks()));
            dto.setYearOfAction(mp.getYearOfAction());
            return dto;
        }else {
            return null;
        }
    }
    public MaintenancePlan toEntity(MaintenancePlanDTO maintenancePlanDTO) {
        MaintenancePlan maintenancePlan = MaintenancePlan.of(maintenancePlanDTO.get_id(),maintenancePlanDTO.getYearOfAction(),maintenanceTaskAssembler.toEntities(maintenancePlanDTO.getTasks())
        ,maintenancePlanDTO.getPlant());
        return maintenancePlan;
    }

}
