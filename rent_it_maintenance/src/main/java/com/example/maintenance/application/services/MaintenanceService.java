package com.example.maintenance.application.services;

import com.example.maintenance.application.dto.MaintenanceTaskDTO;
import com.example.maintenance.domain.model.MaintenanceTask;
import com.example.maintenance.domain.repository.MaintenanceTaskRepository;
import com.example.maintenance.infrastructure.MaintenanceIdentifierFactory;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import java.io.IOException;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Service
public class MaintenanceService {
    @Autowired
    MaintenanceTaskRepository maintenanceTaskRepository;
    @Autowired
    MaintenanceTaskAssembler maintenanceTaskAssembler;
    @Autowired
    MaintenanceIdentifierFactory maintenanceIdentifierFactory;
    @Autowired
    RestTemplate restTemplate;

    public MaintenanceTaskDTO createMaintenanceTask(MaintenanceTaskDTO maintenanceTaskDTO) throws IOException, MessagingException {
        MaintenanceTask maintenanceTask = maintenanceTaskAssembler.toEntity(maintenanceTaskDTO);
        MaintenanceTask savedMainteancneTask = MaintenanceTask.of(maintenanceIdentifierFactory.nextMaintenanceID(),
                maintenanceTask.getDescription(),maintenanceTask.getTypeOfWork(),maintenanceTask.getPrice());
        maintenanceTaskRepository.save(savedMainteancneTask);
        MaintenanceTaskDTO maintenanceTaskDTOWithLink = maintenanceTaskAssembler.toResource(savedMainteancneTask);
        Gson gson = new Gson();
        System.out.println(maintenanceTaskDTOWithLink.getLinks().get(0));
        maintenanceTaskDTO.set_id(maintenanceTaskDTOWithLink.get_id());
        maintenanceTaskDTO.setHref(maintenanceTaskDTOWithLink.getLinks().get(0).getHref());

        RestTemplate rt = new RestTemplate();
        rt.getMessageConverters().add(new StringHttpMessageConverter());
        String response = rt.postForObject(
                "http://localhost:8090/api/reservations",
                maintenanceTaskDTO, String.class);;
        System.out.println("RESPONSE"  + response);

        return maintenanceTaskAssembler.toResource(savedMainteancneTask);
    }
}
