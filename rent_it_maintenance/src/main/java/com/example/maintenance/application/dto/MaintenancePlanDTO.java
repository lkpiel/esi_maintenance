package com.example.maintenance.application.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Data
@Getter
@EqualsAndHashCode
public class MaintenancePlanDTO extends com.example.common.rest.ResourceSupport {

    String _id;
    Integer yearOfAction;

    List<MaintenanceTaskDTO> tasks;
    String plant;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate startDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate endDate;
}
