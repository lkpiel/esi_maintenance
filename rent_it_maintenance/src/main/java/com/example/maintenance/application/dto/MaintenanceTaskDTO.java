package com.example.maintenance.application.dto;

import com.example.maintenance.domain.model.TypeOfWork;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Data
@Getter
@EqualsAndHashCode
public class MaintenanceTaskDTO extends com.example.common.rest.ResourceSupport {
    String _id;
    String description;
    TypeOfWork typeOfWork;
    BigDecimal price;
    String item;
    String href;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate startDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate endDate;
}
